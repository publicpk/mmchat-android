#!/bin/bash

# /////////////////////////////
#
# Build WebRTC for android
#
# See the following links for details
# - http://www.webrtc.org/native-code/development/prerequisite-sw
# - http://www.webrtc.org/native-code/android
# /////////////////////////////

# BUILD_CONFIG=Debug
BUILD_CONFIG="Release"

source ./env_setup_for_android

#
# Sync source
#
#gclient sync

pushd src

#
# SDK, NDK Path
#
source ./build/android/envsetup.sh

if [ "x${BUILD_CONFIG}" == "xRelease" ]; then
    echo ">>> =================================================="
    echo ">>> Release Build"
    GYP_DEFINES="build_with_libjingle=1 build_with_chromium=0 libjingle_java=1 $GYP_DEFINES"
else
    echo ">>> =================================================="
    echo ">>> Debug Build"
    GYP_DEFINES="build_with_libjingle=1 build_with_chromium=0 libjingle_java=1 enable_tracing=1 $GYP_DEFINES"
fi

export GYP_DEFINES
echo ">>> GYP_DEFINES=>${GYP_DEFINES}"

#
# Build all
# need more packages:
#   libxtst-dev libgconf2-dev libgnome-keyring-dev libffi-dev libudev-dev libxss-dev
#
export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=out_android_all"
gclient runhooks
# ninja -C out_android_all/Debug
ninja -C out_android_all/$BUILD_CONFIG

#
# Install 
#
# adb install -r out_android_all/$BUILD_CONFIG/apks/AppRTCDemo.apk
# adb install -r out_android_all/Debug/apks/AppRTCDemoTest.apk
# adb install -r out_android_all/Debug/apks/libjingle_peerconnection_android_unittest.apk
# adb install -r out_android_all/Debug/apks/remote_device_dummy.apk

#
# Build AppRTCDemo app only
#
# export GYP_GENERATOR_FLAGS="$GYP_GENERATOR_FLAGS output_dir=out_android_apprtcpdemo"
# gclient runhooks
# ninja -C out_android_apprtcpdemo/Debug AppRTCDemo
# adb install -r out_android_apprtcpdemo/Debug/apks/AppRTCDemo.apk

popd

# for AppRTCDemo.apk
# PKG=org.appspot.apprtc
# MAIN=.ConnectActivity
# adb shell am start -n $PKG/$MAIN

# for AppRTCDemoTest.apk
# TEST_PKG=org.appspot.apprtc.test
# TEST_RUNNER=android.test.InstrumentationTestRunner
# adb shell am instrument -w $TEST_PKG/$TEST_RUNNER
