#!/bin/bash

# /////////////////////////////
#
# Fetch source and Install dependencies for building WebRTC
#
# See the following links for details
# - http://www.webrtc.org/native-code/development/prerequisite-sw
#
# /////////////////////////////


#
# fetch depot_tools
#
[ ! -d "depot_tools" ] && git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

source ./env_setup_for_ios

#
# Fetch source
#
fetch --nohooks webrtc_ios
gclient sync


#
# Install dependent packages
#
# XCode 3.0 or higher
#
