#!/bin/bash

# /////////////////////////////
#
# Fetch source and Install dependencies for building WebRTC
#
# See the following links for details
# - http://www.webrtc.org/native-code/development/prerequisite-sw
#
# /////////////////////////////

#
# Fetch depot_tools
#
[ ! -d "depot_tools" ] && git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

source ./env_setup_for_android

#
# Fetch source
#
fetch --nohooks webrtc_android
gclient sync


#
# Install dependent packages
#

# full dependencies (recommended)
# pushd src
# ./build/install-build-deps.sh
# popd

# minimal packages
sudo apt-get install g++ python libnss3-dev libasound2-dev libpulse-dev libjpeg62-dev libxv-dev libgtk2.0-dev libexpat1-dev

# for 32-bit build
# sudo apt-get install lib32asound2-dev lib32z1 lib32ncurses5 lib32bz2-1.0

