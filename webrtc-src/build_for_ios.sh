#!/bin/bash

# /////////////////////////////
#
# Build WebRTC for iOS
#
# See the following links for details
# - http://www.webrtc.org/native-code/development/prerequisite-sw
# - http://www.webrtc.org/native-code/ios
# /////////////////////////////

source ./env_setup_for_ios

#
# Sync source
#
gclient sync

export GYP_CROSSCOMPILE=1
export GYP_GENERATORS=ninja

# 32-bit simulator
#export GYP_DEFINES="OS=ios target_arch=ia32"
#export GYP_GENERATOR_FLAGS="output_dir=out_sim"

# 64-bit simulator
#export GYP_DEFINES="OS=ios target_arch=x64"
#export GYP_GENERATOR_FLAGS="output_dir=out_sim64"

# 32-bit device
#export GYP_DEFINES="OS=ios target_arch=arm"
#export GYP_GENERATOR_FLAGS="output_dir=out_ios"

# 64-bit device
export GYP_DEFINES="OS=ios target_arch=arm64"
export GYP_GENERATOR_FLAGS="output_dir=out_ios64"

pushd src

# run the gyp generator script
webrtc/build/gyp_webrtc

gclient runhooks

#ninja -C out_sim/Debug-iphonesimulator AppRTCDemo
#ninja -C out_sim64/Debug-iphonesimulator AppRTCDemo
#ninja -C out_ios/Release-iphoneos AppRTCDemo
ninja -C out_ios64/Release-iphoneos AppRTCDemo

popd


